import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { breeds, GetRandomDogPattern } from './dogs.constants';
import { getRandomArrayElement } from '../utils/array.util';

@Controller()
export class DogsController {

  @MessagePattern(GetRandomDogPattern)
  getRandomDog({ breed }: { breed: string }): string {
    const dogs = breeds[breed];
    if (!dogs) {
      return null;
    }
    return getRandomArrayElement(dogs);
  }
}
