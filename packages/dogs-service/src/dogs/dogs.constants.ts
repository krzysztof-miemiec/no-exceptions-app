export const GetRandomDogPattern = { cmd: 'get_random_dog' };

export const breeds = {
  dachshund: [
    'https://keyassets.timeincuk.net/inspirewp/live/wp-content/uploads/sites/8/2019/05/Stanley-the-Dachshund-70_284814301_502739581-920x613.jpg',
    'https://cdn.royalcanin-weshare-online.io/j_p0tGUBIYfdNSoC0wct/v1/bd4ac1-dachshund-adult-standing-on-path',
    'https://animals.net/wp-content/uploads/2018/07/Dachshund-6-650x425.jpg',
    'https://www.keystonepuppies.com/wp-content/uploads/2018/09/Mini-Dachshund.jpg',
  ],
};
