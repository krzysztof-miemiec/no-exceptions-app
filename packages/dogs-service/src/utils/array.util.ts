export const getRandomArrayElement = <T>(array: T[]): T => {
  const index = Math.round(Math.random() * (array.length - 1));
  return array[index];
};
