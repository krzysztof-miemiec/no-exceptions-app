import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { ApplicationModule } from './app.module';

async function bootstrap() {
  const Config = {
    RMQ: process.env.RABBITMQ_HOST &&
      process.env.RABBITMQ_PORT &&
      `amqp://${process.env.RABBITMQ_HOST}:${process.env.RABBITMQ_PORT}`,
  };

  if (!Config.RMQ) {
    throw new Error('Missing RMQ environmental variables.');
  }

  const app = await NestFactory.createMicroservice(ApplicationModule, {
    transport: Transport.RMQ,
    options: {
      urls: [Config.RMQ],
      queue: 'dogs_queue',
      queueOptions: { durable: false },
    },
  });

  await app.listenAsync();
  console.log('Microservice is running');
}

bootstrap();
