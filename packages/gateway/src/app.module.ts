import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { DogsService } from './dogs.service';
import { ClientsModule } from '@nestjs/microservices';
import { Transport } from '@nestjs/common/enums/transport.enum';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'DOGS_SERVICE',
        transport: Transport.RMQ,
        options: {
          urls: [`amqp://${process.env.RABBITMQ_HOST}:${process.env.RABBITMQ_PORT}`],
          queue: 'dogs_queue',
          queueOptions: { durable: false },
        },
      },
    ]),
  ],
  controllers: [AppController],
  providers: [DogsService],
})
export class AppModule {
}
