import { Controller, Get, Param, Response } from '@nestjs/common';
import { DogsService } from './dogs.service';
import * as express from 'express';

@Controller()
export class AppController {
  constructor(private readonly dogsService: DogsService) {
  }

  @Get('/ok')
  isOk() {
    return {
      date: new Date(),
      version: require('../package.json').version,
    };
  }

  @Get('/dog/:breed')
  async getDog(
    @Param('breed') breed: string,
    @Response() response: express.Response,
  ) {
    const dogUrl = await this.dogsService.getRandomDog(breed);
    response.redirect(303, dogUrl);
  }
}
