import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class DogsService {
  constructor(
    @Inject('DOGS_SERVICE') private readonly client: ClientProxy,
  ) {
  }

  getRandomDog(breed: string): Promise<string> {
    return this.client.send(
      { cmd: 'get_random_dog' },
      { breed: breed || 'dachshund' },
    ).toPromise();
  }

}
