# No Exceptions Sample App

It does nothing at the moment.

## Commands

```bash
./scripts/dependencies.sh # Install binaries needed for development
yarn install              # Install dependencies
yarn build                # Build the app
yarn start                # Start development with Skaffold & TSC Watch
yarn test                 # Run tests
yarn lint                 # Check if syntax is ok
```

## CI Pipeline

```
  TEST     BUILD
  test  -> build
  lint
```
