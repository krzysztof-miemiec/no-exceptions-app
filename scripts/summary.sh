#!/usr/bin/env bash
set -e
# Required input:
# SLACK_HOOK - URL of Slack Incoming Webhook which will receive the notification
# $1 argument - environment
# $2 argument - "success" or "failure"
# CI_COMMIT_SHA - Commit SHA
# CI_COMMIT_MESSAGE - Commit message
# CI_COMMIT_REF_NAME - Branch name

PROJECT_NAME="No Exceptions Sample App"
ENV_NAME=$1
STATUS=$2

echo "Summary"
echo "----------------------------------------"
echo "Project Name:    ${PROJECT_NAME}"
echo "Environment:     ${ENV_NAME:-<not specified>}"
echo "Status:          ${STATUS:-<not specified>}"
echo "Commit:          ${CI_COMMIT_SHA:-<not specified>}"
echo "Message:         ${CI_COMMIT_MESSAGE:-<not specified>}"
echo "Branch:          ${CI_COMMIT_REF_NAME:-<not specified>}"
echo "Slack hook:      ${SLACK_HOOK:-<not specified>}"
echo "Project URL:     ${CI_PROJECT_URL:-<not specified>}"
echo "Pipeline URL:    ${CI_PIPELINE_URL:-<not specified>}"
echo ""

if [[ "${STATUS}" == "success" ]]; then
  MESSAGE="✅ ${PROJECT_NAME} ${CI_COMMIT_REF_NAME} deployment succeeded."
else
  MESSAGE="⛔️ ${PROJECT_NAME} ${CI_COMMIT_REF_NAME} deployment failed. See pipeline information for details."
fi

SLACK_DATA='{"blocks": [
  {
    "type": "section",
    "text": {
      "type": "mrkdwn",
      "text": "'"${MESSAGE}"'"
    }
  },
  {
    "type": "context",
    "elements": [
      {
        "type": "mrkdwn",
        "text": "Environment: *'"${ENV_NAME}"'* | Commit: *'${CI_COMMIT_SHA:-unknown}'* | Branch: *'${CI_COMMIT_REF_NAME:-unknown}'*"
      }
    ]
  },
  {
    "type": "actions",
    "elements": [
      {
        "type": "button",
        "text": {
          "type": "plain_text",
          "text": "GitLab Project Page",
          "emoji": true
        },
        "url": "'${CI_PROJECT_URL}'"
      },
      {
        "type": "button",
        "text": {
          "type": "plain_text",
          "text": "Pipeline Information",
          "emoji": true
        },
        "url": "'${CI_PIPELINE_URL}'"
      }
    ]
  }
]}'

echo $SLACK_DATA

if [[ -z "${SLACK_HOOK}" ]]; then
  echo "😶 Not sending Slack notifications."
  exit 0
fi

curl -X POST \
  ${SLACK_HOOK} \
  -H 'Content-Type: application/json' \
  -d "${SLACK_DATA}"

if [[ "${STATUS}" == "failure" ]]; then
  exit 1
fi
