#!/usr/bin/env bash
set -e

echo "Dependencies installer"
echo "----------------------"
PLATFORM=$([[ $OSTYPE == darwin* ]] && echo darwin || echo linux)
JQ_PLATFORM=$([[ ${PLATFORM} == darwin ]] && echo osx-amd || echo linux)
BIN_DIR=$(pwd)/bin
DEFAULT_GLOBAL_BIN_DIR=/usr/local/bin


mkdir -p bin

function fetch {
  NAME=$1
  URL=$2
  FILE="${BIN_DIR}/${NAME}"
  if [[ -f "$FILE" ]]; then
    echo "→ ${NAME}: 😎 Already exists."
  else
    echo "→ ${NAME}: 📦 Fetching..."
    curl -sSL "${URL}" -o "${FILE}"
    echo "→ ${NAME}: 🎟 Setting executable access rights..."
    chmod +x "${FILE}"
    echo "→ ${NAME}: 👍 Ready!"
  fi
}

# Want to add a curl-downloadable binary? Add it below
fetch kubectl "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/${PLATFORM}/amd64/kubectl"
fetch jq "https://github.com/stedolan/jq/releases/download/jq-1.6/jq-${JQ_PLATFORM}64"
fetch aws-iam-authenticator "https://amazon-eks.s3-us-west-2.amazonaws.com/1.12.7/2019-03-27/bin/${PLATFORM}/amd64/aws-iam-authenticator"
fetch kustomize "https://github.com/kubernetes-sigs/kustomize/releases/download/v3.1.0/kustomize_3.1.0_${PLATFORM}_amd64"
fetch skaffold "https://storage.googleapis.com/skaffold/releases/latest/skaffold-${PLATFORM}-amd64"

# Lets copy binaries to global location
if [[ "$1" == "--copy" ]]; then
  DIR=$2
  if [[ -z "$DIR" ]]; then
    DIR=${DEFAULT_GLOBAL_BIN_DIR}
    echo "⚠ Copy location not specified, defaulting to ${DIR}"
  fi
  echo "Copying binaries to ${DIR}"
  cp -a "${BIN_DIR}/." ${DIR}
else
  echo "Fetched binaries live in ${BIN_DIR} now."
  echo "Append '--copy' to this command to install binaries to a global location, so they could be accessed from anywhere on your machine (default path is ${DEFAULT_GLOBAL_BIN_DIR}, if you want to copy binaries to a different one, just specify it after '--copy' argument)."
fi

export PATH=${BIN_DIR}:$PATH
echo "If you sourced this script, '${BIN_DIR}' directory has been added to your \$PATH"
