#!/usr/bin/env bash
set -e

echo ""
echo "Installing CI dependencies for Alpine Docker..."
echo "----------------------------------------"

function install {
  PACKAGE=$1
  echo "→ ${PACKAGE}: 📦 installing..."
  apk add "${PACKAGE}" >/dev/null
}

install bash
install curl
install python3
install git
install openssh-client
python3 -m ensurepip >/dev/null
pip3 install --upgrade pip setuptools
if [[ ! -e /usr/bin/pip ]]; then ln -s pip3 /usr/bin/pip ; fi
if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi
aws --version >/dev/null 2>&1 || {
  echo "→ aws-cli: 📦 installing..."
  pip3 install awscli --upgrade >/dev/null
  AWS_PATH=$(pip3 show awscli | grep Location | awk '{ print $2 }')
  export PATH=${AWS_PATH}:${PATH}
}

echo ""
echo "🐌 Installing basic dependencies..."
echo "-----------------------------------"
. ./scripts/dependencies.sh
