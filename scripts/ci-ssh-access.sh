#!/usr/bin/env bash
set -e
# This script adds keys needed to access the repository
# Required input:
# GIT_REPOSITORY env var
# SSH_PRIVATE_KEY env var
# SSH_KNOWN_HOSTS env var

if [[ -z "$SSH_PRIVATE_KEY" || -z "$SSH_KNOWN_HOSTS" ]]; then
  echo "⚠ Missing SSH_PRIVATE_KEY or SSH_KNOWN_HOSTS env vars. This build will not have push rights to any repository"
  exit 0
fi

eval $(ssh-agent -s)

echo "🔑 Adding the necessary keys"
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts
git remote set-url origin "${GIT_REPOSITORY}"
git config --global user.email 'no-exceptions-automation@gitlab.com'
git config --global user.name 'No Exceptions Automation'
