#!/usr/bin/env bash
set -e

function performDeployment {
  ./scripts/access-cluster.sh
  ./scripts/deploy.sh --profile=${OVERLAY}
}

performDeployment && STATUS='success' || STATUS='failure'
# ./scripts/summary.sh ${OVERLAY} ${STATUS}
