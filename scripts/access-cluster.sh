#!/usr/bin/env bash
set -e
# Required input
# EKS_CLUSTER_NAME env var
# EKS_ROLE_ARN env var

echo "Configuring Kubernetes Access..."
export KUBECONFIG=~/.kube/config
mkdir ~/.kube
echo "Updating Kubeconfig by AWS CLI..."
aws eks update-kubeconfig --name=${EKS_CLUSTER_NAME} --role-arn=${EKS_ROLE_ARN}
echo "Checking Kubernetes version..."
kubectl version
