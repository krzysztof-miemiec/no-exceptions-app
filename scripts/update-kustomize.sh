#!/usr/bin/env bash
set -e
# Required input:
# OVERLAY env var
# DOCKER_REPOSITORY env var

PROJECT_PATH=$(pwd)

[[ -z "${DOCKER_REPOSITORY}" ]] && echo "🛑 Missing DOCKER_REPOSITORY variable." && exit 1
[[ -z "${OVERLAY}" ]] && echo "🛑 Missing OVERLAY variable." && exit 1

KUBE_ROOT='./.kubernetes'
if [[ ! -d "${KUBE_ROOT}" ]]; then
 echo "🛑 ${KUBE_ROOT} does not exist. Make sure that you're running the script from the project root."
 exit 1
fi

kustomize version >/dev/null 2>&1 || {
  echo "🛑 kustomize is required for the script to run"
  exit 1
}

GIT_TAG=$(git describe --tag)

echo "🎯 Target overlay: ${OVERLAY}"

KUSTOMIZE_PATH="${KUBE_ROOT}/overlays/${OVERLAY}"

echo "Environment overlay '${KUSTOMIZE_PATH}'"
echo "----------------"
echo "Repository:  ${DOCKER_REPOSITORY}"
echo "Tag:         ${GIT_TAG}"
echo ""

# Go to Kustomize location
cd "${KUSTOMIZE_PATH}"
echo "Updating '${KUSTOMIZE_PATH}/kustomization.yaml' with new images"

function updateImage {
  IMAGE=$1
  NEW_IMAGE="${DOCKER_REPOSITORY}/${IMAGE}:${GIT_TAG}"
  echo "${IMAGE} becomes ${NEW_IMAGE}"
  kustomize edit set image "${IMAGE}=${NEW_IMAGE}"
}

updateImage no-exceptions_gateway
updateImage no-exceptions_dogs-service

# Revert location
cd "${PROJECT_PATH}"
