#!/usr/bin/env bash
set -e

HASH=$(git rev-parse --short HEAD)
TAG_NAME=${HASH}

if git tag --list | egrep -q "^$TAG_NAME$"
then
  echo "💔 tag $TAG_NAME already exists, skipping..."
else
  echo "🏷 tagging the current version as $TAG_NAME"
  git tag $TAG_NAME
  git push origin $TAG_NAME
fi
