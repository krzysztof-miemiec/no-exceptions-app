#!/usr/bin/env bash
set -e
# Required input:
# DOCKER_REPOSITORY env var
# OVERLAY env var

./scripts/update-kustomize.sh

skaffold deploy $@

NAMESPACE=no-exceptions-${OVERLAY}

echo "🤞 Deployment is most likely successful"
echo "👀 Let's see how it's going below..."
kubectl get all -n "${NAMESPACE}"
