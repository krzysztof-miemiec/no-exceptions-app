#!/usr/bin/env bash
set -e

echo "Using environmental variables from ${ENVIRONMENT}"
printenv "${ENVIRONMENT}" > .build_env
sed -i -e '/^$/d;s/^/export /' .build_env
chmod 755 .build_env
. .build_env > /dev/null
rm .build_env
