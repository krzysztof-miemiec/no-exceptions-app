#!/usr/bin/env bash
set -e
# Required input:
# DOCKER_REPOSITORY env var
if [ "$DOCKER_REPOSITORY" ]; then REPOSITORY_ARG="--default-repo=${DOCKER_REPOSITORY}"; fi

skaffold build "${REPOSITORY_ARG}" $@
