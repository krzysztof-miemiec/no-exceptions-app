ARG BASE_IMAGE

# This is a base Docker image for all Node.js services
FROM node:11-alpine AS common-base

WORKDIR /usr/app

RUN npm config set unsafe-perm true && \
  npm i -g yarn lerna nodemon

COPY package.json yarn.lock lerna.json ./
RUN yarn install --production

# We may share message types and common clients/utils like below
# COPY packages/common ./packages/common

# Base image for a specific service
FROM ${BASE_IMAGE} AS service
ARG SERVICE_NAME

WORKDIR /usr/app/packages/${SERVICE_NAME}
COPY packages/${SERVICE_NAME}/package.json .
RUN lerna bootstrap -- --production --ignore-optional --frozen-lockfile
COPY packages/${SERVICE_NAME}/dist ./dist

# Development image
FROM service AS service-dev
CMD nodemon ./dist/main.js

# Release image
FROM service AS service-release
RUN addgroup appuser && adduser --disabled-password --ingroup appuser appuser \
  && chmod -R 755 /usr/app
USER appuser
CMD node ./dist/main.js
